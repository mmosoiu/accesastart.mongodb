﻿namespace AccesaStart.Mongo.Models
{
    public class Capacity
    {
        public int Size { get; set; }

        public string Unit { get; set; }
    }
}
