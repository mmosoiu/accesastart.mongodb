﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace AccesaStart.Mongo.Models
{
    public class Herder
    {
        [BsonId]
        public ObjectId Id { get; set; }

        public string Name { get; set; }

        public double Fortune { get; set; }
    }
}
