﻿using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace AccesaStart.Mongo.Models
{
    public class Animal
    {
        [BsonId]
        public ObjectId Id { get; set; }

        public string Name { get; set; }

        public string Color { get; set; }

        public Capacity Capacity { get; set; }

        public int NeckLength { get; set; }

        public int PercentCamel { get; set; }

        public List<string> Gender { get; set; }

        public ObjectId HerderId { get; set; }
    }
}
