﻿using System;
using System.Collections.Generic;
using AccesaStart.Mongo.Models;
using MongoDB.Bson.IO;
using MongoDB.Driver;

namespace AccesaStart.Mongo
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            MongoClient client = new MongoClient();
            IMongoDatabase mongoDatabase = client.GetDatabase("OstrichCamelHerdingDB");

            var animals = mongoDatabase.GetCollection<Animal>("Animals");

            Animal costica = new Animal
            {
                Name = "Costica",
                Capacity = new Capacity
                {
                    Size = 3,
                    Unit = "tonnes"
                },
                Color = "Fuchsia",
                NeckLength = 2,
                PercentCamel = 49,
                Gender = new List<string> { "CamelMale", "BiOstrich" }
            };
            animals.InsertOne(costica);
            //Animal pufina = new Animal
            //{
            //    Name = "Pufina",
            //    Color = "Roz",
            //    NeckLength = 245,
            //    PercentCamel = 2,
            //    Gender = new List<string> { "CamelFluid" }
            //};

            //var costicaUpdate = Builders<Animal>.Update.Set(o => o.Gender, new List<string> { "CamelMale" });

            //animals.UpdateOne(o => o.Name == "Costica", costicaUpdate);

            animals.DeleteOne(ani => ani.Color == "fejfh");

            Console.ReadLine();
        }
    }
}
